#!/usr/bin/perl
use strict;

# Generate all combinations of case for a given wordsList
#
# Usage (default patterns):
#	cat <wordlist> | ./generate.iterateSpecial.pl
# Usage:
#	cat <wordlist> | ./generate.iterateSpecial.pl <findChars> <replacementChars>
#
# --full-only
#	This arg makes it so we only print full replacements
#	 (no partial replacements, all characters that can be replaced are replaced).

my $FULL_REPLACEMENT_ONLY_FLAG = 0;

my $PATTERN = 'aAaAbsS';
my $REPLACEMENT_PATTERN = '@@446$$';

if ($#ARGV >= 0) {
	my $i = 0;
	if ($ARGV[$i + 0] eq '--full-only') {
		$FULL_REPLACEMENT_ONLY_FLAG = 1;
		++$i;
	}
	if ($#ARGV >= 1 + $i) {
		$PATTERN = $ARGV[$i + 0];
		$REPLACEMENT_PATTERN = $ARGV[$i + 1];
	}
}

my %replacers;
for my $i(0 ... length($PATTERN)) {
	my $c1 = substr($PATTERN, $i, 1);
	my $c2 = substr($REPLACEMENT_PATTERN, $i, 1);

	my $tmp = $replacers{$c1};
	if ($tmp) {
		push(@$tmp, $c2);
	} else {
		my @tmpArr = ($c2);
		$tmp = \@tmpArr;
		$replacers{$c1} = $tmp;
	}
}

while(my $word = <STDIN>) {
	chomp($word);

	my $countSpecial = 0;
	my @chars = split('', $word);
	foreach my $char(@chars){
		if($replacers{$char}) {
			++$countSpecial;
		}
	}

	for my $mask(0 .. 2**$countSpecial - 1) {
		if ($FULL_REPLACEMENT_ONLY_FLAG == 1) {
			$mask = 2**$countSpecial - 1;
		}

		# Toggle letter case, every combination of cases
		my @bits = split('', sprintf("%0${countSpecial}b\n", $mask));

		my @strs = ("");
		foreach my $char(@chars){
			my @startArr = ($char);
			my $replacement = \@startArr;
			my $tmpReplacement;
			if($tmpReplacement = $replacers{$char}) {
				if(shift @bits) {
					$replacement = $tmpReplacement;
				}
			}

			# replace char in every str in strs in the position
			my @newStrs = ();
			foreach my $repChar(@$replacement) {
				foreach my $str(@strs) {
					my $newStr = $str . $repChar;
					push(@newStrs, $newStr);
				}
			}
			@strs = @newStrs;
		}
		foreach my $str(@strs) {
			print "$str\n";
		}

		if ($FULL_REPLACEMENT_ONLY_FLAG == 1) {
			last;
		}
	}
}
