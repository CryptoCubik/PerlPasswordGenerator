#!/usr/bin/perl
use strict;
use Algorithm::Combinatorics qw(combinations_with_repetition);

# Usage: cat <wordlist> | ./generate.combineWords.pl [<delimiter> [(<upperBoundNumDelimiters> | <lowerBoundNumDelimiters> <upperBoundNumDelimiters>) [<position>]]]
# i.e.		./generate.combineWords.pl ':' 0 3 | ./generate.iterateSpecial.pl --full-only ':::' '123'
#
# Takes word list on stdin.
#
# <delimiter>			The delimiter you want to use. Suggested value is '\0' since that shouldn't appear in passwords normally.
# <upperBoundNumDelimiters>	The upper limit on the number of delimiters to add.
# <lowerBoundNumDelimiters>	The lower limit on the number of delimiters to add.
# <position>			"left" "middle", or "right"
#				If "left" or "right" no combinations will be formed, only delimiters will be added to the specified side.
# <numberToCombine>		The number of words to combine (default 2).
#
# NOTE: Choosing <position> of "middle" (the default) requires that all words be read in before output can be generated.
#	This is not the case for "left" and "right", they work in a nice streaming fashion.
#
#
# cat linux.words.txt | ./generate.combineWords.pl '\0' 0 3 | ./generate.iterateCase.pl | ./generate.iterateSpecial.pl 'aAaAbsS' '@@446$$' | ./generate.iterateSpecial.pl --full-only '\0\0\0' '123'
#
# cat linux.words.txt | ./generate.combineWords.pl '\0' 0 3 |  ./generate.combineWords.pl '\0' 0 1 'left' |  ./generate.combineWords.pl '\0' 0 1 'right' | ./generate.iterateSpecial.pl 'aAaAbsS' '@@446$$' | ./generate.iterateCase.pl | ./generate.iterateSpecial.pl --full-only '\0\0\0' '123'

my $DELIMITER = ':';
my $UPPER_BOUND_NUM_DELIMITERS = 0;
my $LOWER_BOUND_NUM_DELIMITERS = 0;
my $POSITION = 'middle';
my $NUM_TO_COMBINE = 2;
if ($#ARGV >= 0) {
	my $i = 0;

	$DELIMITER = $ARGV[$i + 0];
	++$i;

	if ($#ARGV >= 0 + $i) {
		$UPPER_BOUND_NUM_DELIMITERS = $ARGV[$i + 0];
	}
	++$i;

	if ($#ARGV >= 0 + $i) {
		$LOWER_BOUND_NUM_DELIMITERS = $UPPER_BOUND_NUM_DELIMITERS;
		$UPPER_BOUND_NUM_DELIMITERS = $ARGV[$i + 0];

		if ($LOWER_BOUND_NUM_DELIMITERS > $UPPER_BOUND_NUM_DELIMITERS) {
			my $tmp = $LOWER_BOUND_NUM_DELIMITERS;
			$LOWER_BOUND_NUM_DELIMITERS = $UPPER_BOUND_NUM_DELIMITERS;
			$UPPER_BOUND_NUM_DELIMITERS = $tmp;
		}
	}
	++$i;

	if ($#ARGV >= 0 + $i) {
		$POSITION = lc($ARGV[$i + 0]);
	}
	++$i;

	if ($#ARGV >= 0 + $i) {
		$NUM_TO_COMBINE = $ARGV[$i + 0];
	}
	++$i;
}

$" = "";
if ($POSITION eq 'middle') {
	my @words = <STDIN>; # Get words from input wordList
	chomp(@words);
	# @words = map { lc $_ } @words; # Lowercase all words

	my $iter = combinations_with_repetition(\@words, $NUM_TO_COMBINE); # All pair combinations of words
	while (my $c = $iter->next) {
		f($c, $LOWER_BOUND_NUM_DELIMITERS, $UPPER_BOUND_NUM_DELIMITERS);
	}
} else {
	while(my $word = <STDIN>) {
		chomp($word);
		for my $i($LOWER_BOUND_NUM_DELIMITERS ... $UPPER_BOUND_NUM_DELIMITERS) {
			my $delims = $DELIMITER x $i;
			if ($POSITION eq 'left') {
				print "${delims}${word}\n";
			} elsif ($POSITION eq 'right') {
				print "${word}${delims}\n";
			} else {
				die "Unknown value for position: <$POSITION>.";
			}
		}
	}
}

sub f {
	my ($comb, $LOWER_BOUND_NUM_DELIMITERS, $UPPER_BOUND_NUM_DELIMITERS) = @_;

	my $popped;
	if ($popped = pop(@$comb)) {
		_f($comb, $popped, $LOWER_BOUND_NUM_DELIMITERS, $UPPER_BOUND_NUM_DELIMITERS);
		push(@$comb, $popped);
	}
	return;
}

sub _f {
	my ($comb, $builtStr, $LOWER_BOUND_NUM_DELIMITERS, $UPPER_BOUND_NUM_DELIMITERS) = @_;

	my $popped;
	if (!($popped = pop(@$comb))) {
		print "$builtStr\n";
		return;
	}

	for my $j($LOWER_BOUND_NUM_DELIMITERS ... $UPPER_BOUND_NUM_DELIMITERS) {
		my $delims = $DELIMITER x $j;
		my $tmpStr = $builtStr . $delims . $popped;
		_f($comb, $tmpStr, $LOWER_BOUND_NUM_DELIMITERS, $UPPER_BOUND_NUM_DELIMITERS);
	}
	push(@$comb, $popped);
}
